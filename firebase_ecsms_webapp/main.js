const firestore = firebase.firestore()

function remove_select_options(list_id) {
    element_name = '#'+list_id
    $(element_name).empty()
}

function set_p_text(text,element_name) {
    var element = document.getElementById(element_name);
    element.innerHTML = text;
}

function disable_elements() {
    // these balance values may not be changed manually
    $("#monthly_balance_cents_amount").prop('disabled', true);
    $("#daily_balance_cents_amount").prop('disabled', true);   
    
    $("#merchant_list").prop('disabled', true);
    $("#country_list").prop('disabled', true);
    $("#card_list").prop('disabled', true);
    $("#monthly_limit_cents_amount").prop('disabled', true);
    $("#daily_limit_cents_amount").prop('disabled', true);       
    $("#submit_changes_merch").prop('disabled', true);
    $("#submit_changes_country").prop('disabled', true);
    $("#submit_changes_limits").prop('disabled', true);      
}

function enable_elements() {
    $("#merchant_list").prop('disabled', false);
    $("#country_list").prop('disabled', false);
    $("#monthly_limit_cents_amount").prop('disabled', false);
    $("#daily_limit_cents_amount").prop('disabled', false);
    $("#submit_changes_merch").prop('disabled', false);
    $("#submit_changes_country").prop('disabled', false);
    $("#submit_changes_limits").prop('disabled', false);     
}

function set_option_text(list_id,text) {
    list_id = '#'+list_id
    $(list_id).append('<option value="none" selected disabled hidden>'+text+'</option>');
}

function get_card_selected_value(){
    fs_populate_merch_list()
    fs_populate_country_list()
    var card_id = document.getElementById("card_list").value;
    fs_get_and_set_employee_card_info(card_id)    
    enable_elements()    
    return card_id
}

function get_employee_selected_value(){
    fs_populate_merch_list()
    fs_populate_country_list()    
    var employee_id = document.getElementById("employee_list").value;
    fs_populate_cards_linked_to_employee(employee_id)
    fs_get_and_set_employee_details(employee_id)
    $("#card_list").prop('disabled', false);        
}

function set_input_value(val,field_name) {
    document.getElementById(field_name).value = val;
}

async function fs_get_and_set_employee_details(employee_id) {
    const snapshot = await firestore.collection('employees').doc(employee_id).get();

    var first_name = snapshot.data()['first_name'];  
    var employment_start_date = snapshot.data()['employment_start_date'];    
    var position = snapshot.data()['position'];      
    var employment_status = snapshot.data()['employment_status'];    
    var email = snapshot.data()['email'];    
    var work_phone = snapshot.data()['work_phone'];    
    
    set_p_text(first_name,'first_name')
    set_p_text(employment_start_date,'employment_start_date')
    set_p_text(position,'position')
    set_p_text(email,'email')
    set_p_text(work_phone,'work_phone')
    set_p_text(employment_status,'employment_status')                
}

async function fs_get_and_set_employee_card_info(card_id) {
    const snapshot = await firestore.collection('employee_cards').doc(card_id).get();
    
    var monthly_limit_cents_amount = snapshot.data()['monthly_limit_cents_amount'];    
    var daily_limit_cents_amount = snapshot.data()['daily_limit_cents_amount'];
    var monthly_balance_cents_amount = snapshot.data()['monthly_balance_cents_amount'];
    var daily_balance_cents_amount = snapshot.data()['daily_balance_cents_amount'];
    var country_group = snapshot.data()['country_group'];
    var merchant_group = snapshot.data()['merchant_group'];    
    
    set_input_value(monthly_limit_cents_amount,'monthly_limit_cents_amount')    
    set_input_value(daily_limit_cents_amount,'daily_limit_cents_amount')
    set_input_value(monthly_balance_cents_amount,'monthly_balance_cents_amount')    
    set_input_value(daily_balance_cents_amount,'daily_balance_cents_amount')   
    
    if(country_group != undefined) {
        $("#country_list").val(country_group);
    }

    if(country_group != undefined) {    
        $("#merchant_list").val(merchant_group);
    }
}

function fs_populate_employee_list(list_id) {
    employee_list = []
    firestore.collection("employees")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            employee_id = doc.id
            first_name = doc.data()['first_name']
            employee_value = employee_id+" - "+first_name
            employee_list[employee_id] = employee_value;
        });
        var list_size = Object.keys(employee_list).length;

        if(list_size == 0) {        
            set_option_text(list_id,'No employees found')

        } else {            
        var select = document.getElementById(list_id);
        for(index in employee_list) {
            select.options[select.options.length] = new Option(employee_list[index], index);
        }
        set_option_text(list_id,'Select an employee ('+list_size+')')   
    }
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

function fs_populate_cards_linked_to_employee(employee_id) {
    card_list = []    
    remove_select_options('card_list')

    firestore.collection("employee_cards").where("employee_id", "==", employee_id)
        .get()
        .then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
            card_id = doc.id
            card_list[card_id] = card_id;
            });
            var list_size = Object.keys(card_list).length;
            if(list_size == 0) {
                set_option_text('card_list','No cards found')
                // disable if employee has no card
            disable_elements()
            } else {
            var select = document.getElementById("card_list");                
            for(index in card_list) {
                select.options[select.options.length] = new Option(card_list[index], index);
            }
            set_option_text('card_list','Select a card ('+list_size+')')                
            }
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });
}

function update_fs_country_values() {

    var card_id = document.getElementById("card_list").value;
    var country_value = document.getElementById("country_list").value;
    
    firestore.collection("employee_cards").doc(card_id).update({'country_group': country_value});
    
    $("#alert_changes_applied_country").fadeTo(2500, 500).slideUp(500, function(){
        $("#alert_changes_applied_country").slideUp(500);
    }); 
}

function update_fs_merchant_values() {

    var card_id = document.getElementById("card_list").value;
    var merchant_value = document.getElementById("merchant_list").value;
    
    firestore.collection("employee_cards").doc(card_id).update({'merchant_group': merchant_value});
    
    $("#alert_changes_applied_merchant").fadeTo(2500, 500).slideUp(500, function(){
        $("#alert_changes_applied_merchant").slideUp(500);
    });         
}

async function update_fs_limit_values() {

    var card_id = document.getElementById("card_list").value;
    var daily_limit_cents_amount = parseInt($('#daily_limit_cents_amount').val());
    var monthly_limit_cents_amount = parseInt($('#monthly_limit_cents_amount').val());  
 
    const doc_ref = firestore.collection('employee_cards').doc(card_id);    
    
    //LIMIT amounts
    doc_ref.update(
        {'daily_limit_cents_amount': daily_limit_cents_amount,
        'monthly_limit_cents_amount': monthly_limit_cents_amount
    });

    // BALANCE amounts - once off set
    monthly_balance_cents_amount_exist = check_field_exist(card_id,'monthly_balance_cents_amount')
    monthly_balance_cents_amount_exist.then(function(value) {
        promise_value = value
        if(promise_value == false) {
            doc_ref.update({'monthly_balance_cents_amount': monthly_limit_cents_amount})
            set_input_value(monthly_limit_cents_amount,'monthly_balance_cents_amount')
        }        
    });  

    daily_balance_cents_amount_exist = check_field_exist(card_id,'daily_balance_cents_amount')     
    daily_balance_cents_amount_exist.then(function(value) {
        promise_value = value
        if(promise_value == false) {
            doc_ref.update({'daily_balance_cents_amount': daily_limit_cents_amount})
            set_input_value(daily_limit_cents_amount,'daily_balance_cents_amount')        
        }            
    });

    //monthly TOTALS - once off set
    monthly_total_cents_amount_exist = check_field_exist(card_id,'monthly_total_cents_amount')
    monthly_total_cents_amount_exist.then(function(value) {
        promise_value = value
        if(promise_value == false) {
            doc_ref.update({'monthly_total_cents_amount': 0})
        }            
    });
    
    daily_total_cents_amount_exist = check_field_exist(card_id,'daily_total_cents_amount')
    daily_total_cents_amount_exist.then(function(value) {
        promise_value = value
        if(promise_value == false) {
            doc_ref.update({'daily_total_cents_amount': 0})
        }            
    });        
    
    $("#alert_changes_applied").fadeTo(2500, 500).slideUp(500, function(){
        $("#alert_changes_applied").slideUp(500);
    });      
}

function fs_populate_merch_list() {

    //clear lists
    remove_select_options('merchant_list')
    set_option_text('merchant_list','Select a merchant group')    

    merchant_list = []
    firestore.collection("merchant_groups")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            merch_group = doc.id
            merchant_list[merch_group] = merch_group;
        });
        var list_size = Object.keys(merchant_list).length;

        if(list_size == 0) {        
            set_option_text('merchant_list','No merchant groups found')
        } else {            
        var select = document.getElementById("merchant_list");
        for(index in merchant_list) {
            select.options[select.options.length] = new Option(merchant_list[index], index);
        }
        set_option_text('merchant_list','Select a merchant group ('+list_size+')')   
    }
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

function fs_populate_country_list() {

    //clear lists
    remove_select_options('country_list')
    set_option_text('country_list','Select a country group')

    country_list = []
    firestore.collection("country_groups")
    .get()
    .then(function(querySnapshot) {
        querySnapshot.forEach(function(doc) {
            country_group = doc.id
            country_list[country_group] = country_group;
        });
        var list_size = Object.keys(country_list).length;

        if(list_size == 0) {        
            set_option_text('country_list','No country groups found')
        } else {            
        var select = document.getElementById("country_list");
        for(index in country_list) {
            select.options[select.options.length] = new Option(country_list[index], index);
        }
        set_option_text('country_list','Select a country group ('+list_size+')')   
    }
    })
    .catch(function(error) {
        console.log("Error getting documents: ", error);
    });
}

async function assign_card_to_employee() {
    var card_id = document.getElementById("card_list_assign").value;    
    firestore.collection("employee_cards").doc(card_id).update({'card_id': card_id});
}

function fs_populate_card_list_assign() {
    card_list = []    
    remove_select_options('card_list_assign')

    //assigned gets set when card is assigned initially
    firestore.collection("cards").where("assigned", "==", false)
        .get()
        .then(function(querySnapshot) {
            querySnapshot.forEach(function(doc) {
            card_id = doc.id
            card_list[card_id] = card_id;
            });
            var list_size = Object.keys(card_list).length;
            if(list_size == 0) {
                set_option_text('card_list_assign','No cards found')           
            } else {
            var select = document.getElementById("card_list_assign");                
            for(index in card_list) {
                select.options[select.options.length] = new Option(card_list[index], index);
            }
            set_option_text('card_list_assign','Select a card ('+list_size+')')                
            }
        })
        .catch(function(error) {
            console.log("Error getting documents: ", error);
        });    
}

function submit_card_assign() {
    var employee_id = document.getElementById("employee_list_assign").value;
    var card_id = document.getElementById("card_list_assign").value;
    
    firestore.collection("employee_cards").doc(card_id).set({'card_id': card_id, 'employee_id':employee_id});    
    firestore.collection("cards").doc(card_id).update({'assigned': true});        

    $("#alert_changes_applied_assign").fadeTo(2500, 500).slideUp(500, function(){
        $("#alert_changes_applied_assign").slideUp(500);
    });     
}

function employee_list_assign_onchange() {
        $("#card_list_assign").prop('disabled', false);
        $("#submit_changes_assign_card").prop('disabled', false);
}

async function check_field_exist(doc_id,field_name) {
    //returns a promise
    field_found = false
    doc_dict = {}

    doc_ref = await firestore.collection('employee_cards').doc(doc_id).get()
    doc_dict = doc_ref.data()

    if(field_name in doc_dict) {
        field_found = true
    }

    return field_found
}
