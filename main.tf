# Create a topic for the after transaction flow
resource "google_pubsub_topic" "after_transaction_topic" {
  name = var.topic
}

# zip up our source code
data "archive_file" "get_token_zip" {
 type        = "zip"
 source_dir  = "${path.root}/functions/get_token/"
 output_path = "${path.root}/functions/get_token/get_token.zip"
}

data "archive_file" "get_transaction_authorization_zip" {
 type        = "zip"
 source_dir  = "${path.root}/functions/get_transaction_authorization/"
 output_path = "${path.root}/functions/get_transaction_authorization/get_transaction_authorization.zip"
}

data "archive_file" "publish_pubsub_topic_zip" {
 type        = "zip"
 source_dir  = "${path.root}/functions/publish_pubsub_topic/"
 output_path = "${path.root}/functions/publish_pubsub_topic/publish_pubsub_topic.zip"
}

data "archive_file" "process_post_transaction_zip" {
 type        = "zip"
 source_dir  = "${path.root}/functions/process_post_transaction/"
 output_path = "${path.root}/functions/process_post_transaction/process_post_transaction.zip"
}

data "archive_file" "get_cake_sage_employees_zip" {
 type        = "zip"
 source_dir  = "${path.root}/functions/get_cake_sage_employees/"
 output_path = "${path.root}/functions/get_cake_sage_employees/get_cake_sage_employees.zip"
}

data "archive_file" "load_json_reference_data_zip" {
 type        = "zip"
 source_dir  = "${path.root}/functions/load_json_reference_data/"
 output_path = "${path.root}/functions/load_json_reference_data/load_json_reference_data.zip"
}

# Create a bucket for the ziped source code
resource "google_storage_bucket" "source_code_bucket" {
  name = "${var.project}${var.source_code_bucket_name}"
}

# Upload the zipped source code to the source code storage bucket
resource "google_storage_bucket_object" "get_token_code" {
  name   = "get_token.zip"
  bucket = google_storage_bucket.source_code_bucket.name
  source = "./functions/get_token/get_token.zip"
}

resource "google_storage_bucket_object" "get_transaction_authorization_code" {
  name   = "get_transaction_authorization.zip"
  bucket = google_storage_bucket.source_code_bucket.name
  source = "./functions/get_transaction_authorization/get_transaction_authorization.zip"
}

resource "google_storage_bucket_object" "publish_pubsub_topic_code" {
  name   = "publish_pubsub_topic.zip"
  bucket = google_storage_bucket.source_code_bucket.name
  source = "./functions/publish_pubsub_topic/publish_pubsub_topic.zip"
}

resource "google_storage_bucket_object" "process_post_transaction_code" {
  name   = "process_post_transaction.zip"
  bucket = google_storage_bucket.source_code_bucket.name
  source = "./functions/process_post_transaction/process_post_transaction.zip"
}

resource "google_storage_bucket_object" "get_cake_sage_employees_code" {
  name   = "get_cake_sage_employees.zip"
  bucket = google_storage_bucket.source_code_bucket.name
  source = "./functions/get_cake_sage_employees/get_cake_sage_employees.zip"
}

resource "google_storage_bucket_object" "load_json_reference_data_code" {
  name   = "load_json_reference_data.zip"
  bucket = google_storage_bucket.source_code_bucket.name
  source = "./functions/load_json_reference_data/load_json_reference_data.zip"
}

# Create a bucket for the ziped source code
resource "google_storage_bucket" "master_data_bucket" {
  name = "${var.project}${var.master_data_bucket_name}"
}

# Create the cloud functions

resource "google_cloudfunctions_function" "get_token_function" {
  name        = "get_token"
  description = "get_token"
  runtime     = "nodejs10"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.source_code_bucket.name
  source_archive_object = google_storage_bucket_object.get_token_code.name
  timeout               = 60
  entry_point           = "get_token"

  trigger_http          = true
  project               = var.project
  region                = var.region

  environment_variables = {
    FUNCTION_BASE_URL = "https://${var.region}-${var.project}.cloudfunctions.net/"
  }

}


resource "google_cloudfunctions_function" "get_transaction_authorization_function" {
  name        = "get_transaction_authorization"
  description = "get_transaction_authorization"
  runtime     = "python37"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.source_code_bucket.name
  source_archive_object = google_storage_bucket_object.get_transaction_authorization_code.name
  timeout               = 60
  entry_point           = "get_transaction_authorization"

  trigger_http          = true
  project               = var.project
  region                = var.region
}


resource "google_cloudfunctions_function" "publish_pubsub_topic_function" {
  name        = "publish_pubsub_topic"
  description = "publish_pubsub_topic"
  runtime     = "python37"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.source_code_bucket.name
  source_archive_object = google_storage_bucket_object.publish_pubsub_topic_code.name
  timeout               = 60
  entry_point           = "publish_pubsub_topic"

  trigger_http          = true
  project               = var.project
  region                = var.region

  environment_variables = {
    GOOGLE_CLOUD_PROJECT = var.project
    TOPIC = google_pubsub_topic.master.name
  }

}



resource "google_cloudfunctions_function" "process_post_transaction_function" {
  name        = "process_post_transaction"
  description = "process_post_transaction"
  runtime     = "python37"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.source_code_bucket.name
  source_archive_object = google_storage_bucket_object.process_post_transaction_code.name
  timeout               = 60
  entry_point           = "process_post_transaction"

  project               = var.project
  region                = var.region

  event_trigger {
    event_type = "google.pubsub.topic.publish"
    resource   = google_pubsub_topic.after_transaction_topic.name
    failure_policy {
      retry = true
    }
  }
}


resource "google_cloudfunctions_function" "get_cake_sage_employees_function" {
  name        = "get_cake_sage_employees"
  description = "get_cake_sage_employees"
  runtime     = "python37"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.source_code_bucket.name
  source_archive_object = google_storage_bucket_object.get_cake_sage_employees_code.name
  timeout               = 60
  entry_point           = "get_employees"

  trigger_http          = true
  project               = var.project
  region                = var.region
}

resource "google_cloudfunctions_function" "load_json_reference_data_function" {
  name        = "load_json_reference_data"
  description = "Function to create a google auth token to call the other functions"
  runtime     = "python37"

  available_memory_mb   = 128
  source_archive_bucket = google_storage_bucket.source_code_bucket.name
  source_archive_object = google_storage_bucket_object.load_json_reference_data_code.name
  timeout               = 60
  entry_point           = "process_file_upload"
  project               = var.project
  region                = var.region
  depends_on = [google_storage_bucket.master_data_bucket]

  event_trigger {
    event_type = "google.storage.object.finalize"
    resource   = "${var.project}${var.master_data_bucket_name}"
    failure_policy {
      retry = true
    }
  }
}

# IAM entry for a single user to invoke the function
resource "google_cloudfunctions_function_iam_member" "publish_invoker" {
  project        = google_cloudfunctions_function.publish_pubsub_topic_function.project
  region         = google_cloudfunctions_function.publish_pubsub_topic_function.region
  cloud_function = google_cloudfunctions_function.publish_pubsub_topic_function.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:${var.project}@appspot.gserviceaccount.com"
}

resource "google_cloudfunctions_function_iam_member" "get_transaction_authorization_function_invoker" {
  project        = google_cloudfunctions_function.get_transaction_authorization_function.project
  region         = google_cloudfunctions_function.get_transaction_authorization_function.region
  cloud_function = google_cloudfunctions_function.get_transaction_authorization_function.name

  role   = "roles/cloudfunctions.invoker"
  member = "serviceAccount:${var.project}@appspot.gserviceaccount.com"
}

resource "google_cloudfunctions_function_iam_member" "token_invoker" {
  project        = google_cloudfunctions_function.get_token_function.project
  region         = google_cloudfunctions_function.get_token_function.region
  cloud_function = google_cloudfunctions_function.get_token_function.name

  role   = "roles/cloudfunctions.invoker"
  member = "allUsers"
}

resource "google_storage_bucket_object" "merchants_upload" {
  name   = "merchants.json"
  bucket = google_storage_bucket.master_data_bucket.name
  source = "./master_data/merchants.json"
  depends_on = [google_cloudfunctions_function.load_json_reference_data_function]  
}

resource "google_storage_bucket_object" "countries_upload" {
  name   = "countries.json"
  bucket = google_storage_bucket.master_data_bucket.name
  source = "./master_data/countries.json"
  depends_on = [google_cloudfunctions_function.load_json_reference_data_function]
}

resource "google_storage_bucket_object" "employees_upload" {
  name   = "employees.json"
  bucket = google_storage_bucket.master_data_bucket.name
  source = "./master_data/employees.json"
  depends_on = [google_cloudfunctions_function.load_json_reference_data_function]  
}

resource "google_storage_bucket_object" "country_groups_upload" {
  name   = "country_groups.json"
  bucket = google_storage_bucket.master_data_bucket.name
  source = "./master_data/country_groups.json"
  depends_on = [google_cloudfunctions_function.load_json_reference_data_function]
}

resource "google_storage_bucket_object" "merchant_groups_upload" {
  name   = "merchant_groups.json"
  bucket = google_storage_bucket.master_data_bucket.name
  source = "./master_data/merchant_groups.json"
  depends_on = [google_cloudfunctions_function.load_json_reference_data_function]  
}