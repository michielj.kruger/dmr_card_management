# Welcome to the ECSMS : Employee Card and Spend Management System Setup

<walkthrough-author name="Michiel Kruger, Ron George and Dean Lambrechts" repositoryUrl="https://gitlab.com/michielj.kruger/dmr_card_management" tutorialName="ECSMS : Employee Card and Spend Management System"></walkthrough-author>
<walkthrough-tutorial-duration duration=15></walkthrough-tutorial-duration>

## Introduction

This tutorial will guide you through setting up the ECSMS : Employee Card and Spend Management System Setup using terraform and google cloud platform.

The following resources will be deployed

- Google storage buckets
- Google cloud functions
- Google Pub/Sub topic
- Cloud Firestore
- Firebase Web App
- BigQuery

You will need a Google Cloud Platform project with billing enabled to set up ECSMS.  You should however fall within the free tier, but it will depend on the usage on your projects and billing accounts.  Make sure that you understand Google's pricing and free tier on the above mentioned resources. 

When you are ready, click start to continue.

## Step 1 - Select a project and login

### Create Project

<walkthrough-project-billing-setup></walkthrough-project-billing-setup>

### Set project as default

``` bash
gcloud config set project {{project-id}}
```

### Authenticate the session

``` bash
gcloud auth login
```

### Enable Api's

``` bash
gcloud services enable cloudbuild.googleapis.com
```

``` bash
gcloud services enable pubsub.googleapis.com
```

``` bash
gcloud services enable cloudfunctions.googleapis.com
```

``` bash
gcloud services enable firestore.googleapis.com
```

``` bash
gcloud services enable appengine.googleapis.com
```

## Step 2 - Generate Key for Service Account

We need to generate a key to use during authentication when making calls to our cloud functions. The script below will create a key which you need to keep safe.

``` bash
gcloud iam service-accounts keys create ~/key.json --iam-account {{project-id}}@appspot.gserviceaccount.com
```
<walkthrough-editor-open-file filePath="key.json">
    Open key.json
</walkthrough-editor-open-file>

Copy the private key and paste it into the *key* variable of your Investec .env file.

## Step 3

### Update the SAGE Url and API Key

Update the SAGE Url and API Key by opening the below file and replacing "your-domain" with the subdomain for your CakeHR instance and the "token" with your API Key.

Note:  Do not add quotes to the values

Be sure to save the file before you continue

<walkthrough-editor-open-file filePath="cloudshell_open/dmr_card_management/functions/get_cake_sage_employees/config.txt">
    Open config.txt
</walkthrough-editor-open-file>

Or open the file in the following path to edit the config

cloudshell_open\dmr_card_management\functions\get_cake_sage_employees\config.txt

## Step 4

### Initialize firestore

``` bash
gcloud app create --region=us-central
```

``` bash
gcloud alpha firestore databases create --region=us-central
```

## Step 5

### Initialize Terraform

``` bash
terraform init
```

### Apply the Terraform script

You can change the topic variable if you want a different topic name.

``` bash
terraform apply -var topic="transactions" -var project="{{project-id}}" -var region="us-central1" -var source_code_bucket_name="_source_code" -var master_data_bucket_name="_master_data" 
```

## Step 6 - Set up the UMD (User Managed Data) front end

### Initialize firebase

``` bash
firebase init
```

### Select the folowing options in the console

Select *Hosting: Configure and deploy Firebase Hosting sites*  (Note: You must hit spacebar on the option to select it before pressing enter to submit)

### Project setup

Select *Add Firebase to an existing Google Cloud Platform project*

Select the project you are currently working with

### Select following options

When presented with the *What do you want to use as your public directory?* Change the default (public) folder to the below by selecting the below folder name

``` bash
firebase_ecsms_webapp
```

Select N for *Configure as a single-page app (rewrite all urls to /index.html)*


Select N for *Set up automatic builds and deploys with GitHub?*


Select N for *File firebase_ecsms_webapp/index.html already exists. Overwrite?*


### Deploy the app

``` bash
firebase deploy
```

## Step 7

### Setup firebase rules

Go to [Firestore rules](https://console.firebase.google.com/project/{{project-id}}/firestore/rules) and paste the below code to the rules section and click on publish.

``` json
rules_version = '2';
// Allow read/write access to all users under any conditions
// Warning: NEVER use this rule set in production; it allows
// anyone to overwrite your entire database.
service cloud.firestore {
  match /databases/{database}/documents {
    match /{document=**} {
      allow read, write: if true;
    }
  }
}
```

### Bookmark the app link

This is the Hosting URL. You will use this link later in the setup process.  https://{{project-id}}.web.app

## Complete

### Conclusion

Your GCP services are now set up. You can now return to the GitLab project to complete the setup. 

