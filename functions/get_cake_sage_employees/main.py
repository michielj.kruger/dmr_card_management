import requests
import logging
from firebase_admin import firestore
import configparser
import time


def get_employees(request):

    global db
    db = firestore.Client()

    # Read our Sage config
    config_parser = configparser.RawConfigParser()
    config_parser.read(r'config.txt')

    url = config_parser.get('SAGE','url')
    token = config_parser.get('SAGE','token')

    print(f'url -> {url}       Token -> {token}')

    headers = {'X-Auth-Token': token, 'Content-Type': 'application/json', 'Accept': 'application/json'}
    request = requests.get(url,headers=headers)

    request_json = request.json()

    last_seen_date = int(time.time())

    #Write to firestore
    write_to_firestore(request_json, last_seen_date)

    #Check if any employees should be terminated.
    #Used to cater for the fact that Sage simply removes employees from the API call if they are terminated
    terminate_employees(last_seen_date)

def write_to_firestore(request_json, last_seen_date):

    for employee in request_json['data']:
        employee['last_seen_date'] = last_seen_date
        employee['terminated'] = False
        db.collection('employees').document(str(employee['id'])).set(employee)


def terminate_employees(last_seen_date):
    doc_ref = db.collection('employees')
    field_updates = {'terminated': True}

    results = db.collection('employees').where('last_seen_date', '!=', last_seen_date).get()

    for item in results :
        doc = doc_ref.document(item.id)
        doc.update(field_updates)