import logging 
from firebase_admin import firestore

def get_transaction_authorization(request):

    global db
    db = firestore.Client()             # pylint: disable=maybe-no-member
    authorized = False
    authorization_reason = ''

    request_json = request.get_json(silent=True)
    # logging.info(request_json)
    
    card_id = request_json['card']['id'] #string
    transaction_cents_amount = request_json['centsAmount'] #int
    transaction_merchant_code = request_json['merchant']['category']['code'] #string
    transaction_country_code = request_json['merchant']['country']['code'] #string


    logging.info(card_id)

    # Check if the card exists in the card collection
    card_exists = check_card_details(card_id, request_json)

    #Check if the card has been assigned to an employee
    if not card_exists :
        authorized = False
        authorization_reason = 'Unsuccessful; Card not found'
        return {'authorization':authorized,'authorization_reason':authorization_reason}
    else :

        #Get card details ( Monthly and Daily balance as well as whether the employee is terminated)
        daily_balance_cents_amount, monthly_balance_cents_amount, employee_is_terminated, employee_country_group, employee_merchant_group \
            = get_employee_card_details(card_id)


        #Check if merchant and country are allowed
        transaction_group_allowed, transaction_group_reason = check_country_and_merchant(employee_country_group, employee_merchant_group,transaction_merchant_code, transaction_country_code)


        if (employee_is_terminated) :
            authorized = False
            authorization_reason = 'Unsuccessful: Employee is terminated!'
        elif (transaction_group_allowed == False) :
            authorized = False
            authorization_reason = 'Unsuccessful: ' + transaction_group_reason
        elif (transaction_cents_amount > daily_balance_cents_amount) or (transaction_cents_amount > monthly_balance_cents_amount):
            authorized = False
            authorization_reason = 'Unsuccessful: Transaction cents amount of '+str(transaction_cents_amount)+' greater than daily balance cents amount of '+str(daily_balance_cents_amount) + ' or monthly balance cents amount of '+str(monthly_balance_cents_amount)
        else :
            authorized = True
            authorization_reason = 'Transaction Authorised'

        return {'authorization':authorized,'authorization_reason':authorization_reason}

def get_employee_card_details(card_id):
    doc_ref = db.collection('employee_cards').document(card_id)

    daily_balance_cents_amount = doc_ref.get({'daily_balance_cents_amount'}).to_dict()['daily_balance_cents_amount']
    monthly_balance_cents_amount = doc_ref.get({'monthly_balance_cents_amount'}).to_dict()['monthly_balance_cents_amount']
    employee_id = doc_ref.get({'employee_id'}).to_dict()['employee_id']
    country_group = doc_ref.get({'country_group'}).to_dict()['country_group']
    merchant_group = doc_ref.get({'merchant_group'}).to_dict()['merchant_group']

    #Check if the employee is terminated
    employee_is_terminated = check_employee_is_terminated(employee_id)

    return daily_balance_cents_amount, monthly_balance_cents_amount, employee_is_terminated, country_group, merchant_group


def check_employee_is_terminated(employee_id):
    doc_ref = db.collection('employees').document(employee_id)
    employee_is_terminated = doc_ref.get({'terminated'}).to_dict()['terminated']
    return employee_is_terminated
    


def check_card_details(card_id, request_json):
    doc_ref = db.collection('cards').document(card_id)

    doc = doc_ref.get()

    if doc.exists:
        return True
    else:
        card_json = request_json['card']
        card_json['assigned'] = False   # Set assigned to False 
        doc_ref.set(
            card_json
        )
        return False


def check_country_and_merchant(employee_country_group, employee_merchant_group,transaction_merchant_code, transaction_country_code):
    doc_ref = db.collection('merchant_groups').document(employee_merchant_group)
    allowed_merchant_code = doc_ref.get({'merchant_code'}).to_dict()['merchant_code']

    doc_ref = db.collection('country_groups').document(employee_country_group)
    allowed_country_code = doc_ref.get({'country_code'}).to_dict()['country_code']

    transaction_allowed = False

    if (transaction_merchant_code != allowed_merchant_code) :
        transaction_allowed = False
        transaction_reason = 'Merchant not authorised!'
        print(f'Merchant not approved - {transaction_merchant_code}  <->  {allowed_merchant_code}')
    elif (transaction_country_code != allowed_country_code) :
        transaction_allowed = False
        transaction_reason = 'Country not authorised!'
        print(f'Country not approved - {transaction_country_code}  <->  {allowed_country_code}')
    else :
        transaction_allowed = True
        transaction_reason = ''
        print(f'Allowed - {transaction_country_code}  <->  {allowed_country_code}   <->   {transaction_merchant_code}  <->  {allowed_merchant_code}')
    
    return transaction_allowed, transaction_reason
