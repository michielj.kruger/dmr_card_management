from google.cloud import storage
from firebase_admin import firestore
import json 
import logging 


def process_file_upload(event, context):
     """Triggered by a change to a Cloud Storage bucket.
     Args:
          event (dict): Event payload.
          context (google.cloud.functions.Context): Metadata for the event.
     """
     data = event
     #logging.info(data)

     file_name = data['name']
     
     file_root = file_name.split(".", 1)[0]
     file_extension = file_name.split(".", 1)[1]
     
     #logging.info("Processing file: " + file_name + "    Extension: " + file_extension)

     if (file_extension == "json"):
          client = storage.Client()

          bucket = data['bucket']

          json_file = client.get_bucket(bucket).blob(file_name)          
          json_data = json.loads(json_file.download_as_string(client=None))     

          db = firestore.Client()

          for doc in json_data:

               #logging.info(doc)
               doc_id = doc['code']
               doc_ref = db.collection(file_root).document(doc_id)
               doc_ref.set(doc)

     else:
          logging.info("NOT json file")
