import base64
import logging 
import json
from firebase_admin import firestore
from datetime import datetime
#from google.cloud import bigquery

def process_post_transaction(event, context):
     """Triggered from a message on a Cloud Pub/Sub topic. test
     Args:
          event (dict): Event payload.
          context (google.cloud.functions.Context): Metadata for the event.
     """

     global db
     db = firestore.Client()            # pylint: disable=maybe-no-member

     pubsub_message = base64.b64decode(event['data']).decode('utf-8')
     #pubsub_message = '{"accountNumber":"10011532825","dateTime":"2020-11-26T19:17:30.218Z","centsAmount":300,"currencyCode":"zar","type":"card","reference":"simulation","card":{"id":"1d8bff04a3540f2e914fc1148b53c4e3547105d69f98ae45ba335c7c6e96e5a5"},"merchant":{"category":{"code":"5462","key":"bakeries","name":"Bakeries"},"name":"Ron test 004","city":"Cape Town","country":{"code":"ZA","alpha3":"ZAF","name":"South Africa"}}}'
     
     logging.info('PubSub message -> ' + pubsub_message)

     request_json = json.loads(pubsub_message)
     #logging.info(request_json)

     card_id = request_json['card']['id'] #string
     transaction_cents_amount = request_json['centsAmount'] #int
     currency_code = request_json['currencyCode'] #int

     #logging.info(card_id)
     #logging.info('Transaction Amount -> ' + currency_code + ' ' + str(transaction_cents_amount))

     daily_balance_cents_amount, monthly_balance_cents_amount, daily_total_cents_amount, monthly_total_cents_amount = get_balances(card_id)

     new_daily_balance_cents_amount = daily_balance_cents_amount - transaction_cents_amount
     new_monthly_balance_cents_amount = monthly_balance_cents_amount - transaction_cents_amount
     new_daily_total_cents_amount = daily_total_cents_amount + transaction_cents_amount
     new_monthly_total_cents_amount = monthly_total_cents_amount + transaction_cents_amount   

     #logging.info( str(daily_balance_cents_amount)  + '  -  ' +str(monthly_balance_cents_amount)  + '  -  ' +str(daily_total_cents_amount)  + '  -  ' +str(monthly_total_cents_amount))
     #logging.info( str(new_daily_balance_cents_amount)  + '  -  ' +str(new_monthly_balance_cents_amount)  + '  -  ' +str(new_daily_total_cents_amount)  + '  -  ' +str(new_monthly_total_cents_amount))

     # Update our balances
     update_balances(card_id, new_daily_balance_cents_amount, new_monthly_balance_cents_amount, new_daily_total_cents_amount, new_monthly_total_cents_amount)

     # Write the transaction to the transactions table
     write_transaction(request_json)

     # Stream to Biq Query
     #stream_bq(request_json)


def get_balances(card_id):

    doc_ref = db.collection('employee_cards').document(card_id)

    monthly_balance_cents_amount = doc_ref.get({'monthly_balance_cents_amount'}).to_dict()['monthly_balance_cents_amount']
    daily_balance_cents_amount = doc_ref.get({'daily_balance_cents_amount'}).to_dict()['daily_balance_cents_amount']
    monthly_total_cents_amount = doc_ref.get({'monthly_total_cents_amount'}).to_dict()['monthly_total_cents_amount']
    daily_total_cents_amount = doc_ref.get({'daily_total_cents_amount'}).to_dict()['daily_total_cents_amount']

    return daily_balance_cents_amount, monthly_balance_cents_amount, daily_total_cents_amount, monthly_total_cents_amount

def update_balances(card_id, new_daily_balance_cents_amount, new_monthly_balance_cents_amount, new_daily_total_cents_amount, new_monthly_total_cents_amount) :
     doc_ref = db.collection('employee_cards').document(card_id)

     doc_ref.update({
          'daily_balance_cents_amount': new_daily_balance_cents_amount,
          'monthly_balance_cents_amount': new_monthly_balance_cents_amount,
          'daily_total_cents_amount': new_daily_total_cents_amount,
          'monthly_total_cents_amount': new_monthly_total_cents_amount
     })

def write_transaction(request_json) :
     doc_ref = db.collection('transactions').document()   

     doc_ref.set(
          request_json
     )
     
"""
def stream_bq(data) :
     bqClient = bigquery.Client()
     dataset_ref = bqClient.dataset('raw')
     table_ref = dataset_ref.table('transactions')
     table = bqClient.get_table(table_ref)

     #insert
     errors = bqClient.insert_rows(table, [data])       

     if len(errors) != 0:
          print(data,errors)
"""