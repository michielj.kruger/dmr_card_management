import base64
import json
import os
import logging 

from google.cloud import pubsub_v1     

# Instantiates a Pub/Sub client
publisher = pubsub_v1.PublisherClient()
PROJECT_ID = os.getenv('GOOGLE_CLOUD_PROJECT')
TOPIC_NAME = os.getenv('TOPIC')

# Publishes a message to a Cloud Pub/Sub topic.
def publish_pubsub_topic(request):
    
    request_json = json.dumps(request.get_json(silent=True))
    
    # References an existing topic
    topic_path = publisher.topic_path(PROJECT_ID, TOPIC_NAME)               # pylint: disable=maybe-no-member
    
    message_bytes = request_json.encode('utf-8')

    # Publishes a message
    try:
        publish_future = publisher.publish(topic_path, data=message_bytes)
        publish_future.result()  # Verify the publish succeeded
        return 'Message published.'
    except Exception as e:
        print(e)
        return (e, 500)
