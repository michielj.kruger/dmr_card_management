"use strict";

const { google } = require("googleapis");

exports.get_token = async (req, res) => {
  if (!req.body.email || !req.body.key || !req.body.functionName) {
    res
      .status(500)
      .send(
        'Missing parameter(s); include "email", "key" and "functionName" properties in your request.'
      );
    return;
  }

  let functionName = process.env.FUNCTION_BASE_URL + req.body.functionName;
  
  const client = new google.auth.JWT({
    email: req.body.email,
    key: req.body.key,
  });
  let token = await client
    .fetchIdToken(functionName)
    .then((idToken) => {
      return idToken;
    })
    .catch((error) => {      
      console.log(error.message);
      res.status(500).send(error.message);
    });

  return res.status(200).send(token);
};
