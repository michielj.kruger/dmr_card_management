# ECSMS : Employee Card and Spend Management System
 
## Background
 
![alt text](./DMR-Diagram.jpg "DMR-Diagram")
 

### Creators

Dean Lambrechts
Michiel Kruger
Ron George
 
## How to setup
 
### Payroll system setup
 
We used CakeHR by SAGE to demo this integration but this can be easily changed to any other system with an open API.  Local on premise legacy systems can also be used by scheduling downloads of the employee information and uploading it to a google storage bucket.  Some changes will need to be made to integrate with other systems.
 
Use this [link](https://cake.hr/) to get a CakeHR account.  At the time of writing this readme SAGE offered a 14 day free trial. 

CakeHR will start your account with some demo employees but you need to select *start fresh* at the top of the screen to get access to the API Integrations tab.

To enable the API follow the steps in this [link](https://support.cake.hr/en/articles/3246469-how-does-cakehr-api-work)

Add employees by following the steps in this [link](https://support.cake.hr/en/articles/3905505-manually-add-an-employee) 

In the next steps you will need your SAGE subdomain, you can get that in your unique Url when you log into you your newly created sage company. The bold subdomain below is what you will need.

**subdomain**.sage.hr

You will also need you API key created above in the next steps. 


### GCP setup

Use the following tutorial to set up the Employee Card Management System
 
[![Open this project in Cloud Shell](http://gstatic.com/cloudssh/images/open-btn.png)](https://console.cloud.google.com/cloudshell/open?git_repo=https://gitlab.com/michielj.kruger/dmr_card_management.git&page=editor&tutorial=tutorial.md)
 

### Investec setup
 
Copy the following code into your env.json.  Replacing "project-id" , "region-id" and "key" with the values obtained during the previous step.

``` js
{
    "email": "<project-id>@appspot.gserviceaccount.com",
    "key": "<key>",
    "authorizationUrl": "https://<region-id>-<project-id>.cloudfunctions.net/get_transaction_authorization",
    "tokenUrl": "https://<region-id>-<project-id>.cloudfunctions.net/get_token",
    "transactionUrl": "https://<region-id>-<project-id>.cloudfunctions.net/publish_pubsub_topic"
}
```

Copy the following code into your main.js.

``` js
// This function runs before a transaction.
const beforeTransaction = async (authorization) => {
    
    let response = false;
    
    let token = await getToken("get_transaction_authorization");

    console.log(token);

    if (token) 
    {
        response = await getTransactionAuthorization(authorization, token);
    };

    console.log(authorization);
    console.log(response);
     
    return response.authorization;
    
};

// Retrieve token 
async function getToken(functionNameToCall) {
    try {
        let response = await fetch(process.env.tokenUrl, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json'},
            body: JSON.stringify({email: process.env.email, key: process.env.key, functionName: functionNameToCall})
        });

        return response.text();
    } catch(error) {
        console.log(error);
    }
};


// Post transaction
async function postTransaction(transaction, token) {
    try {
        let response = await fetch(process.env.transactionUrl, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}`},
            body: JSON.stringify(transaction)
        });

        const result = await response.text();
        console.log(result);
    } catch(error) {
        console.log(error);
    }
};


// Post transaction
async function getTransactionAuthorization(authorization, token) {
    console.log(token);
    console.log(authorization);
    console.log(process.env.authorizationUrl);
    try {
        let response = await fetch(process.env.authorizationUrl, {
            method: 'POST',
            headers: { 'Content-Type': 'application/json', 'Authorization': `Bearer ${token}`},
            body: JSON.stringify(authorization)
        });

        console.log(response);
        const result = await response.json();
        console.log(result);
        return result
    } catch(error) {
        console.log("In error logging");
        console.log(error);
        console.log(result);
        console.log(response);
        return false
    }
    
};

// This function runs after a transaction was successful.
const afterTransaction = async (transaction) => {    
    console.log(transaction);
    
    let token = await getToken("publish_pubsub_topic");

    if (token) 
    {
        await postTransaction(transaction, token);
    }
};
 
```


### Simulate a transaction
 
Since we cannot access cards programmatically we won't be able to allocate a card to an employee until the first transaction took place.  
 
Simulate a transaction on Investec now. This will add the simulation card to the card collection in Firestore.  The transaction will be declined.


### Assign a card to an employee using the UI and set the spend limits
 
Access the link below replacing the [project-id] with your project id
 
https://[project_id].web.app/
 
Refer to the [video](https://youtu.be/6gKs0gLEiUQ) demo on how to link a card to an employee and set the spend limits.


### Complete

You have now successfully set up the solution. Simulate a transaction based on the logic you defined above
 
 
## Future prospects

We are sure that this solution will give you a good idea of what can be achieved with programmable banking and automated integrations with HR and payroll systems.  There is still a lot that we would like to add to this solution going forward.  Please see below a list.

- Integrate with other payroll systems
- Add currency to the validations
- Add temporary employee spend logic to authorise once-off transactions and travel spend for a specified period
- Add an interface to update merchant and country category groups
- Build in spend reporting
- Integrate with accounting software/tools 

 
## Investec requests
 
This project relies on the below data that at the time of writing this readme were not programmatically accessible.  It will be great if Investec could add this data to the Open API.
 
- Cards assigned to an Investec account/profile
- merchants.json
- country-codes.json
 
 
## Acknowledgements
 
### Programmable Banking Community open source projects used
 
We made use of the [Investec Command Center](https://gitlab.com/bezchristo/investec-command-center) built by Christo Bezuidenhout.  We ended up changing the Cloud Functions to Python 3.7 runtime for consistency purposes.



