variable "topic" {
    type = string
    default = "transactions"
}

variable "project" {
    type = string
    default = "dmr_card_management"
}

variable "source_code_bucket_name" {
    type = string
    default = "_source_code"
}

variable "master_data_bucket_name" {
    type = string
    default = "_master_data"
}

variable "region" {
    type = string
    default = "us-central1"
}
